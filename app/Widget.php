<?php

namespace App;
use App\Web_widget;
use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    public function Webs(){
        return $this->belongsToMany(Web::class);
    }
    public function Template(){
        return $this->belongsTo(Template::class);
    }

}
