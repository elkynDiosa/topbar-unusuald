<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
    public function Widgets(){
        return $this->belongsToMany(Widget::class);
    }
}
