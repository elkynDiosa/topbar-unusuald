<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public function Widgets(){
        return $this->hasMany(Widget::class);
    }
}
