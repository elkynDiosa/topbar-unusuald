<?php

namespace App\Http\Controllers;

use App\Widget_type;
use Illuminate\Http\Request;

class WidgetTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Widget_type  $widget_type
     * @return \Illuminate\Http\Response
     */
    public function show(Widget_type $widget_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Widget_type  $widget_type
     * @return \Illuminate\Http\Response
     */
    public function edit(Widget_type $widget_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Widget_type  $widget_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Widget_type $widget_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Widget_type  $widget_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Widget_type $widget_type)
    {
        //
    }
}
