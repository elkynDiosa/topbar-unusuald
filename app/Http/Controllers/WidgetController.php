<?php

namespace App\Http\Controllers;

use App\Widget;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Web;
use App\Web_widget;
use stdClass;

class WidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $widget = Widget::where("user_id", $user_id)->orderBy("id", "DESC")->get();
        return response()->json($widget);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id)
    {
        $widget = Widget::find($id);
        $widget->status = !$widget->status;
        $widget->update();
        return response()->json(true);
    }
    public function store(Request $request)
    {
        //ADD NEW WIDGET
        $widget = new Widget();
        $widget->name = $request->name;
        //CREATE JSON OF DATA
        $widget->data = '{"message": "' . $request->message . '","url": "' . $request->url . '", "text_url": "' . $request->text_url . '", "theme": "' . $request->theme . '", "position": "' . $request->position . '"}';
        $widget->widget_type_id = 1; //NOW IS ONE, EXIST A WIDGET TYPE ACTIVE
        $widget->template_id = 1;
        $widget->always_show = $request->alwaysShow;
        $widget->status = true;
        $widget->user_id = Auth::user()->id;
        $widget->slog = Uuid::uuid();
        $widget->script = '<script src="http://localhost/topbar/public/script/initialize.js" token="' . $widget->slog . '" defer=""></script>';
        $widget->save();

        $web = new Web();
        $web->user_id = Auth::user()->id;
        $web->url = $request->web;
        if ($request->webSelect == "Personalize") {
            $all = false;

            $include = $request->websArray;
            $array = "";
            for ($i = 0; $i < count($include); $i++) {
                if ($i == 0) {
                    $array .= '"' . $include[$i] . '"';
                } else {
                    $array .= ', "' . $include[$i] . '"';
                }
            }
        } else {
            $all = true;
            $array = "";
        }
        $objet = '{"all": "' . $all . '","include": [' . $array . '], "except":[]}';
        $web->rules = $objet;
        $web->save();

        $web_widget = new Web_widget();
        $web_widget->web_id = $web->id;
        $web_widget->widget_id = $widget->id;
        $web_widget->save();
        return $widget->script;
    }

    public function show($slog)
    {
        $widget = Widget::where('slog', '=', $slog)->orderBy('id', 'DESC')->first();
        return response()->json($widget);
    }

    public function getWidgetWeb($slog)
    {
        $widget = Widget::where('slog', '=', $slog)->first();
        $web = $widget->Webs;
        $template = $widget->template;
        return response()->json([
            'web' => $web,
            'widget' => $widget,
            'template' => $template
        ]);
    }

    public function update(Request $request, Widget $widget)
    {
        //ADD NEW WIDGET
        $widget->name = $request->name;
        //CREATE JSON OF DATA
        $widget->data = '{"message": "' . $request->message . '","url": "' . $request->url . '", "text_url": "' . $request->text_url . '", "theme": "' . $request->theme . '", "position": "' . $request->position . '"}';
        $widget->always_show = $request->alwaysShow;
        $widget->update();

        $web_array = $widget->Webs;
        foreach ($web_array as $web) {
            $web->url = $request->web;
            if ($request->webSelect == "Personalize") {
                $all = false;
                $include = $request->websArray;
                $array = "";
                for ($i = 0; $i < count($include); $i++) {
                    if ($i == 0) {
                        $array .= '"' . $include[$i] . '"';
                    } else {
                        $array .= ', "' . $include[$i] . '"';
                    }
                }
            } else {
                $all = true;
                $array = "";
            }
            $objet = '{"all": "' . $all . '","include": [' . $array . '], "except":[]}';
            $web->rules = $objet;
            $web->update();
        }
        return response()->json(true);
    }

    public function destroy(Widget $widget)
    {
        //
    }
}
