<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/template/{id}', 'TemplateController@show');
Route::get('/change/status/{id}', 'WidgetController@changeStatus');
Route::get('/widget/webs/{slog}', 'WidgetController@getWidgetWeb');
Route::apiResource('templates', 'TemplateController');
Route::apiResource('widgets', 'widgetController');
