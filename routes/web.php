<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Widget;

Route::group(['middleware' => 'web'], function () {
    Route::get('/{option?}/{op?}', function () {
        return view('home');
    });
    Auth::routes();
});
