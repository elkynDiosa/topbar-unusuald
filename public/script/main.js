// Title Component   
class TitleComponent extends HTMLElement {

    template = HTMLElement.prototype

    mounted = null;

    // Elements
    title_wrapper_mobile = HTMLElement.prototype
    title_wrapper_desktop = HTMLElement.prototype
    title_mobile = HTMLElement.prototype
    title_desktop = HTMLElement.prototype

    // Atributes
    THIS_SCRIPT = document.currentScript;
    TOKEN = document.currentScript.getAttribute("token");
    THEME = document.currentScript.getAttribute("data_theme");
    MESSAGE = document.currentScript.getAttribute("data_message");
    URL = document.currentScript.getAttribute("data_url");
    TEXT_URL = document.currentScript.getAttribute("data_text_url");

    //default theme
    TEMPLATE= "";
    TEXT_COLOR = "";
    _titleMobile = this.URL;
    _titleDesktop = this.URL;
    
    constructor() {
        super();
        // this.getDateTemplate();
        //defining colors by theme
        switch (this.THEME) {
            case 'Dark':
                this.TEXT_COLOR = '#FFFFFF';
                break;
            case 'Ligth':
                this.TEXT_COLOR = '#000000';
                this.ICON = this.ICON_LIGTH;
                break;
            default:
                console.log('We are sorry, at the moment we do not have the necessary data');
        }

        this.template = document.createElement("template");
        this.template.innerHTML = `
            <style>
            .title__container--desktop {
                text-align: center;
                height: 60px;
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
            }

            .title__container--desktop a {
                font-size: 15px;
                color: #878787;
                width: auto;
                margin-left: 30px;
                padding: 5px 25px;
                color: #ffffff;
                text-decoration: none;
                background: #E91E63;
                border-radius: 6px;
                transition: all .5s ease;   
            }

            .title__container--desktop a:hover {
                color: #000000;
                animation: title .2s;    
            }

            .title__container--mobile {
                display: none;
            }
            
            .title__container--mobile a{
                font-size: 12px;
                height: 60px;
                color: #878787;
                display: flex;
                justify-content: center;
                align-items: center;
                text-decoration: none;
            }
    
            .subscribe__text--mobile{
                margin: 0 .5em;
            }
            .text_message{
                width: auto;
                color: ${this.TEXT_COLOR};
            }

            @media only screen and (max-width: 550px) {
                .title__container--desktop {
                    margin-bottom:30px;
                }
                .icon_close {
                    position: relative;
                    top: -80px;
                    left: 90%;
                }

            }
            </style>
           
            `;
        // Create Elements

        // Title mobile
        this.title_wrapper_mobile = document.createElement('div');
        this.title_wrapper_mobile.classList.add('title__container--mobile');
        this.title_mobile = document.createElement('a');
        this.title_mobile.setAttribute('href', this.URL);

        // Title desktop
        this.title_wrapper_desktop = document.createElement('div');
        this.title_wrapper_desktop.classList.add('title__container--desktop')
        this.title_desktop = document.createElement('a');
        this.title_desktop.classList.add('title_desktop')
        this.title_desktop.setAttribute('target', '_blank');
        this.title_desktop.setAttribute('href', this.URL);

        this.message_show = document.createElement('p');
        this.message_show.setAttribute('class', 'text_message');

        // TAKE ATTRIBUTE
        this.title_mobile.innerText = this._titleMobile;
        this.title_desktop.innerText = this._titleDesktop;
        this.message_show.innerText = this.MESSAGE;

        // EXPORT THE TEMPLATE
        this.title_wrapper_desktop.appendChild(this.message_show);
        this.title_wrapper_mobile.appendChild(this.title_mobile);
        this.title_wrapper_desktop.appendChild(this.title_desktop);

        this.attachShadow({ mode: "open" });
        this.shadowRoot.innerHTML = this.template.innerHTML;
        this.shadowRoot.appendChild(this.title_wrapper_mobile);
        this.shadowRoot.appendChild(this.title_wrapper_desktop);


        this.title_mobile.innerText = this._titleMobile;
        this.title_desktop.innerText = this.TEXT_URL;

        this._desk = this.title_wrapper_desktop.querySelector(".title_desktop")
    }

    connectedCallback() {
        

    }
    getTemplate(theme_id){
        return new Promise(
            (resolve, reject) => {
                fetch("http://localhost/topbar/public/api/template/" + theme_id)
                    .then((data) => data.json())
                    .then((data) => resolve(data))
            });
    }

    getDateTemplate = async () => {
        try {
            await this.getTemplate(this.THEME).then(data => { this.TEMPLATE = data });
        } catch (error) {

        }
    }
    
    setState(globalComp) {

        var _newDesk = null;
        var _newMobile = null;

        this._nd = globalComp.titleDesktop

        if (globalComp.titleDesktop) {
            _newDesk = globalComp.titleDesktop
            console.log(_newDesk)
        }

        if (globalComp.titleMobile) {
            _newMobile = globalComp.titleMobile
            console.log(_newMobile)
        }

        if (_newDesk) {
            console.log(this.shadowRoot.querySelector(".title_desktop"))
            this._desk.textContent = _newDesk
            return false;
        }

        // console.log(this._desk.textContent)

        return true;
    }
}

class TopbarComponent extends HTMLElement {

    template = HTMLElement.prototype

    stateGlobal = null;
    //icon of close
    ICON_LIGTH = '<svg height="10pt" viewBox="0 0 329.26933 329" width="10pt" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>'
    ICON_DARK = '<svg height="10pt" viewBox="0 0 365.71733 365" width="10pt" xmlns="http://www.w3.org/2000/svg"><g fill="#f44336"><path d="m356.339844 296.347656-286.613282-286.613281c-12.5-12.5-32.765624-12.5-45.246093 0l-15.105469 15.082031c-12.5 12.503906-12.5 32.769532 0 45.25l286.613281 286.613282c12.503907 12.5 32.769531 12.5 45.25 0l15.082031-15.082032c12.523438-12.480468 12.523438-32.75.019532-45.25zm0 0"/><path d="m295.988281 9.734375-286.613281 286.613281c-12.5 12.5-12.5 32.769532 0 45.25l15.082031 15.082032c12.503907 12.5 32.769531 12.5 45.25 0l286.632813-286.59375c12.503906-12.5 12.503906-32.765626 0-45.246094l-15.082032-15.082032c-12.5-12.523437-32.765624-12.523437-45.269531-.023437zm0 0"/></g></svg>'


    // Atributes
    THIS_SCRIPT = document.currentScript;
    TOKEN = document.currentScript.getAttribute("token");
    THEME = document.currentScript.getAttribute("data_theme");
    POSITION = document.currentScript.getAttribute("data_position");

    _titleMobile = this.URL;
    _titleDesktop = this.URL;

    //defining colors by theme
    ICON = this.ICON_DARK;
    BACKGROUND_COLOR = '#000000';
    POSITION_TOPBAR = 'top: 0px';

    constructor() {
        super();

        switch (this.THEME) {
            case 'Dark':
                this.BACKGROUND_COLOR = '#000000';
                this.ICON = this.ICON_DARK;
                break;
            case 'Ligth':
                this.BACKGROUND_COLOR = '#FFFFFF';
                this.ICON = this.ICON_LIGTH;

                break;
            default:
                console.log('We are sorry, at the moment we do not have the necessary data');
        }
        switch (this.POSITION) {
            case 'Top':
                this.POSITION_TOPBAR = 'top: 0px'
                break;
            case 'Bottom':
                this.POSITION_TOPBAR = 'bottom: 0px'
                break;
            default:
                console.log('We are sorry, at the moment we do not have the necessary data');
        }

        this.template = document.createElement("template");
        this.template.innerHTML = `
            <style>
            @import url('https://fonts.googleapis.com/css?family=Kulim+Park|Montserrat&display=swap');
            :host {
                --primary-color: #212121;
                --secondary-color: #980093; 
                --hover-color: #0A8A9E;
                --text-color: #FFFFFF;
            }
            .hide{
                display: none !important;
            }
            .topbar-component {
                min-height: 60px;
                width: 100%;     
                background: ${this.BACKGROUND_COLOR}; 
                display: flex;
                justify-content: space-around;
                position: fixed;
                ${this.POSITION_TOPBAR};
            }
            a{
                font-size: 30px;
            }
            .title-component{
                width: 90%
            }       
            .icon_close {
                position: relative;
       
                background-color: transparent;
                border:none;
                cursor: pointer;
                border-radius: 2px;
                transition: all .4s;
            }
            .icon_close:hover{
                transform: rotate(90deg);
            }
            </style>    
        `;

        this.divGlobal = document.createElement('div');
        this.divGlobal.setAttribute('class', 'topbar-component');
        this.divGlobal.setAttribute('id', 'topbar-component');
        this.titleComponent = document.createElement("title-component");
        this.titleComponent.setAttribute('class', 'title-component')

        //ADD CLOSE ICON
        this.icon_close = document.createElement('button');
        this.icon_close.classList.add('icon_close');
        this.icon_close.id = "icon_close";
        this.icon_close.onclick = () => this.close();
        this.icon_close.innerHTML = this.ICON;


        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.template.innerHTML;
        this.divGlobal.appendChild(this.titleComponent);
        this.divGlobal.appendChild(this.icon_close);
        this.shadowRoot.appendChild(this.divGlobal);
    }


    connectedCallback() {
    }
    close() {
        this.divGlobal.classList.add("hide");
        let issetCookies = document.cookie.indexOf('topbar');
        if(issetCookies == -1){
            document.cookie= "TOPBAR = true";
        }
    }
    open() {
        this.classList.remove("hide");
    }
}


// window.customElements.define('subscribe-component', SubscribeComponent);
window.customElements.define('title-component', TitleComponent);
window.customElements.define('topbar-component', TopbarComponent);

// var globalComp = document.createElement("topbar-component");
var globalComp = document.createElement("title-component");

globalComp.setState({
    _titleDesktop: 'the OTHER new title',
    _titleMobile: 'the OTHER new desktop'
})