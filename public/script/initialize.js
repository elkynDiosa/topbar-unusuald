const THIS_SCRIPT = document.currentScript;
const TOKEN = document.currentScript.getAttribute("token");
const LIBR_URL = "http://localhost/topbar/public/script/main.js";
//topbar variables
var topbar_data = {};
var web_config = {};
var template = {};
//get data for topbar and dynamically add

var getDataTopBar = (token) => {
    return new Promise(
        (resolve, reject) => {
            fetch("http://localhost/topbar/public/api/widget/webs/" + token)
                .then((data) => data.json())
                .then((data) => resolve(data))
        });
};
var getConfigTopBar = (token) => {
    return new Promise(
        (resolve, reject) => {
            fetch("http://localhost/topbar/public/api/widget/webs/" + token)
                .then((resp) => resp.json())
                .then((resp) => resolve(resp))
        });
};

//main template where the webElement will work 
var addTemplate = () => {
    return new Promise((resolve, reject) => {
        try {
            var topbarComponent = document.createElement("topbar-component");
            document.body.appendChild(topbarComponent);
            if (document.body.contains(topbarComponent)) {
                resolve();
            } else {
                throw "Head not contains css link";
            }
        } catch (error) {
            reject(error);
        }
    });
};

var addScriptSource = (data) => {
    let data_i = JSON.parse(data.data);
    return new Promise((resolve, reject) => {
        try {
            var script_src = document.createElement("script");
            script_src.setAttribute("src", LIBR_URL);
            script_src.setAttribute("type", "text/javascript");
            script_src.id = TOKEN;
            script_src.setAttribute("data_message",  data_i.message);
            script_src.setAttribute("data_position",  data_i.position);
            script_src.setAttribute("data_url",  data_i.url);
            script_src.setAttribute("data_theme", data_i.theme);
            script_src.setAttribute("data_text_url",  data_i.text_url);
            //script_src.setAttribute("parent", "#app");
            document.head.appendChild(script_src);
            if (document.head.contains(script_src)) {
                resolve();
            } else {
                throw "Head not contains css link";
            }
        } catch (error) {
            //reject(error);
        }
    });
};

var initWidget = async () => {
    try {
        await getDataTopBar(TOKEN).then(data => { topbar_data = data.widget, web_config = data.web, template = data.template});
        let data = JSON.parse(this.topbar_data.data);
        data.theme =JSON.parse(this.template.data);
        let show2 = true;
        let issetCookies = document.cookie.indexOf('TOPBAR');
        if (this.topbar_data.always_show == 1) {
            show2 = true;
        }else if(issetCookies == -1){
            show2 = true;
        }else{
            show2 = false;
        }
        if (show2) {
            let rules = JSON.parse(this.web_config[0].rules);

            let path_url = window.location.pathname;
            let show = true;
            if (rules.all != 1) {
                for (let i = 0; i < rules.include.length; i++) {
                    if (path_url.indexOf(rules.include[i])  == 1 || path_url == "/") {
                        show = true;
                        i = rules.include.length + 1;
                    } else {
                        show = false;
                    }
                }
            } else {
                show = true;
            }
            if (show) {
                await addTemplate();
                // await addStyleSource(topbarObjet.teme);
                await addScriptSource(this.topbar_data).then(async () => {
                    let interval = setInterval(() => {
                        let script = document.getElementById(TOKEN);
                        if (document.head.contains(script)) {
                            // console.log("Head contains this script")
                            if (script.initTopBarWidget) {
                                script.initTopBarWidget(topbar_data);
                                clearInterval(interval);
                            } else {

                            }

                        } else {
                            //console.log("Not contains this script")
                        }
                    }, 1000);
                });
            }
        }
        // console.log("Has loaded", topbar_data);
    } catch (error) {
        // console.error("Error", error);
    }

};

initWidget();
