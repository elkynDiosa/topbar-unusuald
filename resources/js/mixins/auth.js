let user = document.head.querySelector('meta[name="user"]')
if(user.content){
    let currentUser = JSON.parse(user.content);
}
let metaToken = document.head.querySelector('meta[name="csrf-token"]');
let token = {"_token": metaToken.content};
module.exports = {
    data() {
        return {
        }
    },
    computed: {
        user() {
            return JSON.parse(user.content);
        },
        check() {
            return !!user.content;
        },
        guest() {
            return ! this.check;
        }
    },
    methods: {
        logout() {
            let url =  "/logout"
            axios.post(url, token).then(response => {
                location.href = "/";
            }).catch(error =>{
                console.log(error)
            });
        }
    }
}