require('./bootstrap');
window.Vue = require('vue');
Vue.component('app', require('./components/AppComponent.vue').default);
Vue.component('navegation', require('./components/NavegationComponent.vue').default);
Vue.component('table-subscribers', require('./components/analytics/TableSubscribersComponent.vue').default);
Vue.component('notification-not-data', require('./components/analytics/NotificationNotData.vue').default);
Vue.component('widgets-user', require('./components/WidgetsComponent.vue').default);
Vue.component('home', require('./components/HomeComponent.vue').default);
// import { ValidationProvider, extend } from 'vee-validate';
// import { required } from 'vee-validate/dist/rules';
// Vue.use(ValidationProvider,extend);
import auth from './mixins/auth';
Vue.mixin(auth);
import Vuetify from 'vuetify';
Vue.use(Vuetify);
import router from './router.js'
const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify({
        theme: {
          themes: {
            light:{
              primary: "#242424",
              secondary: "#BDBDBD",
              accent: "#000000",
              error:" #FF5252",
            }
          },
        },
        icons: {
          iconfont: 'mdiSvg', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
        },
      }) 
});
