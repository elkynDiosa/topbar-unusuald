import Vue from 'vue'

import Router from 'vue-router'
Vue.use(Router)


//function to redirect and block views
function guardMyroute(to, from, next)
{
var isAuthenticated= false;
let user = document.head.querySelector('meta[name="user"]')
if(user.content){
  	isAuthenticated = true;
}else{
	isAuthenticated= false;
}

 if(isAuthenticated) 
 {
  next(); // allow to enter route
 } 
 else
 {
  next('/login'); // go to '/login';
 }
}
export default new Router({
	routes: [
		{
			path: '/',
			name: 'home',
			component: require('./views/home').default,
			meta: {title: 'Home'}
		},
		{
			path: '/analytics',
			name: 'analytics',
			beforeEnter : guardMyroute,
			component: require('./views/analytics').default,
			meta: {title: 'Analytics'}
		},
		{
			path: '/widgets',
			name: 'widgets',
			beforeEnter : guardMyroute,
			component: require('./views/widgets').default,
			meta: {title: 'Widgets'}
		},
		{
			path: '/new-widget',
			name: 'new-widget',
			beforeEnter : guardMyroute,
			component: require('./views/NewWidget').default,
			meta: {title: 'New-Widget'}
		},
		{
			path: '/edit-widget/:slog',
			name: 'edit-widget',
			beforeEnter : guardMyroute,
			component: require('./views/EditWidget').default,
			props: true,
			meta: {title: 'Edit'}
		},
		{
			path: '/login',
			name: 'login',
			component: require('./views/Login').default,
			props: true
		},
		{
			path: '/signup',
			name: 'signup',
			component: require('./views/Signup').default,
			props: true
		}
	],
	mode: 'history'
})
