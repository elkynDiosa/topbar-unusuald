<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->json('data');
            $table->string('script');
            $table->boolean('always_show');
            $table->boolean('status');
            $table->foreignId('widget_type_id')->constrained();
            $table->foreignId('template_id')->constrained();
            $table->foreignId('user_id')->constrained();

            $table->uuid('slog');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widgets');
    }
}
