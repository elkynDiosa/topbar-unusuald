<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 6)->create();
        factory(App\Template::class, 6)->create();
        factory(App\Web::class, 5)->create();
        factory(App\Widget_type::class)->create(['name'=>'TopBar']);
        factory(App\Widget_type::class)->create(['name'=>'Modal']);
        factory(App\Widget_type::class)->create(['name'=>'Form']);
        factory(App\Widget_type::class)->create(['name'=>'Chat']);
        factory(App\Widget::class, 5)->create();
    }
}
