<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Widget;
use Faker\Generator as Faker;

$factory->define(Widget::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'data'=> '{"theme": "dark", "position": "top", "message":  "visit our new page. For design lovers",
              "url": "https://www.bypeople.com/", "text_url":"Watch now"}',
        'script' => $faker->domainName,
        'always_show' => $faker->boolean(),
        'status' => $faker->boolean(),
        'widget_type_id' => rand(1, 4),
        'template_id' => rand(1, 4),
        'user_id' => rand(1, 4),
        'slog' => $faker->uuid()
    ];
});
