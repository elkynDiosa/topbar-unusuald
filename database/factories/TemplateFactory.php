<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Template;
use Faker\Generator as Faker;

$factory->define(Template::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'data' => '{
          "global":  "dark",
          "tille": "top",
          "topbar":  "View from 15th Floor"
      }',
    ];
});
