<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Web;
use Faker\Generator as Faker;

$factory->define(Web::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 5),
        'url' => $faker->domainName,
        'rules' => '{"all": "true", "include":[], "except":  []}'

    ];
});
